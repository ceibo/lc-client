(function () {
	'use strict';

	var addToFavoriteCtrl  = function($scope, $resource) {
		var ctrl = this;	
		var queryParams = {};
		queryParams[ ctrl.idAs || 'id'] = ctrl.id;

		if(!ctrl.titleAdd){
			ctrl.titleAdd = 'Añadir a favoritos';
		}

		if(!ctrl.titleRemove){
			ctrl.titleRemove = 'Quitar de favoritos';
		}

		if(ctrl.id){
			if(ctrl.user){
				ctrl.user.uid = ctrl.user.uid || ctrl.user.id;
				if(ctrl.service) {
					ctrl.loading = true;
					if(ctrl.serviceAsPromise) {
						ctrl.service.check(ctrl.user, ctrl.id)
							.then(function(data){
								if(data){
									ctrl.data = data;
									ctrl.inFavorite = data;
								}

								!ctrl.inFavorite ? ctrl.title = ctrl.titleAdd : ctrl.title = ctrl.titleRemove;
								ctrl.loading = false;
							});	
					} else {
						ctrl.service.check(ctrl.user, ctrl.id, function(data){
							if(data){
								ctrl.data = data;
							}
							ctrl.loading = false;
						});
					}
				} else {
					// no service
				}
			}else{
				//no user
			}
		}else{
			//no id
			
		}

		//functions
		ctrl.setToFavorite = function(inFavorite){
			if(ctrl.user && ctrl.user.uid && ctrl.service && ctrl.service.set){
				ctrl.loading = true;
				ctrl.service.set(ctrl.user.uid, ctrl.id, inFavorite)
					.then(function(data){
						$scope.$parent.user.favorites = data.favorites;
						!inFavorite ? ctrl.inFavorite = true : ctrl.inFavorite = false;
						!ctrl.inFavorite ? ctrl.title = ctrl.titleAdd : ctrl.title = ctrl.titleRemove;
						ctrl.loading = false;
					});
			}
		};

		/*css*/
		/*
		.fav-group{
			&.overlay{
				position: absolute;
				display: block;
				top: 5px;
				right: 5px;
				z-index: 9999;
				.opacity(.95);
			}
		}

		*/
	};

	angular
		.module('lc.components.favorites.add-to-favorite', [])
		.controller('addToFavoriteCtrl', addToFavoriteCtrl)
		.directive('addToFavorite', function() {
	        // Runs during compile
	        return {
	            scope: {
	                id: '=',
	                user: '=',
	                service: '=',
	                serviceAsPromise: '=',
	                titleInline: '=',
	                overlay: '=',
	                titleAdd: '=',
	                titleRemove: '='
	            }, // {} = isolate, true = child, false/undefined = no change
	            controller: 'addToFavoriteCtrl as ctrl',
	            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
	            restrict: 'EA', // E = Element, A = Attribute, C = Class, M = Comment
	            templateUrl: '../scripts/components/favorites/templates/add-to-favorite.html',
	            bindToController: true
	        };
	    });
}());