(function(){
	'use strict';
	angular.module('utils', [])

	.directive("scroll", function ($window) {
		return function(scope, element, attrs) {
			angular.element($window).bind("scroll", function() {
				if (this.pageYOffset >= 150) {
					scope.boolChangeClass = true;
				} else {
					scope.boolChangeClass = false;
				}
				scope.$apply();
			});
		};
	})

	.directive('selectOnClick', ['$window', function ($window) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attrs) {
	            element.on('click', function () {
	                if (!$window.getSelection().toString()) {
	                    // Required for mobile Safari
	                    this.setSelectionRange(0, this.value.length);
	                }
	            });
	        }
	    };
	}])

	.service('anchorSmoothScroll', function(){
			
		var service = this;

		service.scrollTo = function(eID) {
			var startY = currentYPosition();
			var stopY = elmYPosition(eID);
			var distance = stopY > startY ? stopY - startY : startY - stopY;
			if (distance < 100) {
				scrollTo(0, stopY); return;
			}
			var speed = Math.round(distance / 100);
			if (speed >= 20) {
				speed = 20;
			}
			var step = Math.round(distance / 25);
			var leapY = stopY > startY ? startY + step : startY - step;
			var timer = 0;
			if (stopY > startY) {
				for ( var i=startY; i<stopY; i+=step ) {
					setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
					leapY += step; if (leapY > stopY) leapY = stopY; timer++;
				} return;
			}
			for ( var i=startY; i>stopY; i-=step ) {
				setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
				leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
			}

			function currentYPosition() {
				// Firefox, Chrome, Opera, Safari
				if (self.pageYOffset) { 
					return self.pageYOffset;
				}
				// Internet Explorer 6 - standards mode
				if (document.documentElement && document.documentElement.scrollTop){
					return document.documentElement.scrollTop;
				}
				// Internet Explorer 6, 7 and 8
				if (document.body.scrollTop) {
					return document.body.scrollTop;
				}
				return 0;
			}

			function elmYPosition(eID) {
				if(eID){
					var elm = document.getElementById(eID);
					var y = elm.offsetTop;
					var node = elm;
					while (node.offsetParent && node.offsetParent != document.body) {
						node = node.offsetParent;
						y += node.offsetTop;
					} return y;
				}else{
					return 0;
				}
			}
		};
	})

	.filter('range', function() {
  		return function(input, total) {
    		total = parseInt(total);
    		for (var i=0; i<total; i++) {
    		    input.push(i);
    		}
    		return input;
  		};
	});

})();