(function(){

	'use strict';
	var homeCtrl = function($scope, data, userService) {
		var home = this;

		home.productsHighlighted = data.productsHighlighted;
		home.slider = data.slider;
		home.user = $scope.$parent.main.user;
		
		home.services = {
			userService : {
				check: userService.checkFavorite,
				set: userService.setFavorite
			}
		};

		function init() {
			
		}

		//INITIALIZING
		init();
	};

	angular
		.module('homeModule')
			.controller('homeCtrl', homeCtrl);
})();
