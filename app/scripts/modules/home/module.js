(function(){
	'use strict';
	angular.module('homeModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('app.lc.home', {
				url: '/',
				controller : 'homeCtrl as home',
				templateUrl: '/scripts/modules/home/views/home.html',
				resolve : {
					/*slider : function(productService) {
						return productService.retrieveSlider()
					},
					productsHighlighted : function(productService) {
						return productService.retrieveHighlighted()
					}*/
					data : function(productService, $q) {
						var dataPromise = $q.defer();
						productService.retrieveSlider().then(function(firstQueryData){
							productService.retrieveHighlighted().then(function(secondQueryData){
								dataPromise.resolve({slider:firstQueryData, productsHighlighted: secondQueryData});
							});
						});

						return dataPromise.promise;
					}
				}
			});
	})

	.directive('lcSlider', function() {
		return {
			restrict: 'E',
			transclude: true,
			scope: {products: '='},
			templateUrl: 'template/slider.html',
			link: function(scope, element, attrs, controllers) {
				
			}
		};
	});
})();