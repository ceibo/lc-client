(function(){

	'use strict';
	var subcategoryCtrl = function($scope, products, $stateParams, userService) {
		var subcategory = this;

		subcategory.products = products;
		subcategory.title = $stateParams.categoryName + ' - ' + $stateParams.subcategoryName;
		
		subcategory.user = $scope.$parent.main.user;
		
		subcategory.services = {
			userService : {
				check: userService.checkFavorite,
				set: userService.setFavorite
			}
		};

		function init() {
			
		}

		//INITIALIZING
		init();
	};

	angular
		.module('subcategoryModule')
			.controller('subcategoryCtrl', subcategoryCtrl);
})();
