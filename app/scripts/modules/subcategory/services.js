(function(){

	'use strict';

	var subcategoriesService = function(fireRef, Kutral, $q, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var subcategorySchema;

		var subcategoryModel;

		subcategorySchema = new Kutral.Schema({
			'title': {type: String, indexOn: true}
		});

		subcategoryModel = kutral.model('subcategories', subcategorySchema);

		service.retrieveAll = function(){
			var subcategoryRetriveAllPromise = $q.defer();

			/*subcategoryModel.find().populate("subsubcategorys").$asArray(function(data){
				subcategoryRetriveAllPromise.resolve(data);
			});*/

			var ref = new Firebase(fireRef + 'subcategories');
			subcategoryRetriveAllPromise.resolve($firebaseArray(ref));

			return subcategoryRetriveAllPromise.promise;
		};

		service.getSubcategoryById = function(id){
			var productGetSubcategoryByIdPromise = $q.defer();
			
			subcategoryModel.find({$id:id}, function(data){
				productGetSubcategoryByIdPromise.resolve(data);
			});

			return productGetSubcategoryByIdPromise.promise;
		};

		service.getSubcategoryByTitle = function(tit){
			var productGetSubcategoryByTitlePromise = $q.defer();
			subcategoryModel.find({title:tit}, function(data){
				productGetSubcategoryByTitlePromise.resolve(data);
			});

			return productGetSubcategoryByTitlePromise.promise;
		};
	};
	 
	angular
		.module('subcategoryModule')
			.service('subcategoriesService', subcategoriesService);

})();