(function(){
	'use strict';
	angular.module('subcategoryModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.subcategory', {
				url: '/categorias/:categoryName/:subcategoryName/:categoryId/:subcategoryId',
				templateUrl: '/scripts/modules/subcategory/views/subcategory.html',
				controller : 'subcategoryCtrl as subcategory',
				resolve : {
					products : function(productService, $stateParams) {
						var categoryId = $stateParams.categoryId;
						var subcategoryId = $stateParams.subcategoryId;
						return productService.getProductsBySubCategory(categoryId, subcategoryId);
					}
				}
			});
	});
})();