(function(){
    'use strict';
    angular.module('brandsModule', [])

    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('app.lc.brand', {
                url: '/marca/:brandName/:brandId',
                templateUrl: '/scripts/modules/brands/views/brand.html',
                controller : 'brandCtrl as brand',
                resolve : {
                    products : function(productService, $stateParams) {
                        var brandId = $stateParams.brandId;
                        return productService.getProductsByBrand(brandId);
                    }
                }
            });
    });
})();
