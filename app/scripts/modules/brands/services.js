(function(){

    'use strict';

    var brandsService = function(fireRef, Kutral, $q, $firebaseArray) {
        var service = this;

        var kutral = new Kutral(fireRef);
        var brandSchema;

        var brandModel;

        brandSchema = new Kutral.Schema({
            'title': {type: String, indexOn: true}
        });

        brandModel = kutral.model('brands', brandSchema);

        service.retrieveAll = function(){
            var brandRetriveAllPromise = $q.defer();

            var ref = new Firebase(fireRef + 'brands');
            brandRetriveAllPromise.resolve($firebaseArray(ref));

            return brandRetriveAllPromise.promise;
        };

        service.getBrandsHighlighted = function(){
            var getBrandsHighlight = $q.defer();

            brandModel.find({ highlight : true }).$asArray(function(data){
                getBrandsHighlight.resolve(data);
            });

            return getBrandsHighlight.promise;
        };

    };

    angular
        .module('brandsModule')
            .service('brandsService', brandsService);
})();
