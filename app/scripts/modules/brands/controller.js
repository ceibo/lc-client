(function(){

    'use strict';
    var brandCtrl = function($scope, products, $stateParams, userService) {
        var brand = this;

        brand.products = products;
        brand.title = $stateParams.brandName;
        brand.user = $scope.$parent.main.user;

        brand.services = {
            userService : {
                check: userService.checkFavorite,
                set: userService.setFavorite
            }
        };

        function init() {

        }

        //INITIALIZING
        init();
    };

    angular
        .module('brandsModule')
            .controller('brandCtrl', brandCtrl);
})();
