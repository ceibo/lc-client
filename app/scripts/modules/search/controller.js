(function(){

	'use strict';
	var searchCtrl = function($scope, searchProducts, userService) {
		var search = this;

		if(!searchProducts || searchProducts.length === 0){
			search.notFound = true;
		}

		search.services = {
			userService : {
				check: userService.checkFavorite,
				set: userService.setFavorite
			}
		};

		search.searchProducts = searchProducts;
		search.user = $scope.$parent.main.user;

		function init() {
			
		}

		//INITIALIZING
		init();
	};

	angular
		.module('searchModule')
			.controller('searchCtrl', searchCtrl);
})();
