(function(){
	'use strict';
	angular.module('searchModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.search', {
				url: '/busqueda',
				templateUrl: '/scripts/modules/search/views/search.html',
				controller : 'searchCtrl as search',
				resolve : {
					searchProducts : function(productService){
						return productService.getSearchProducts();
					}
				}
			});
	});
})();