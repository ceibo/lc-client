(function(){

	'use strict';
	var categorieCtrl = function($scope, products, $stateParams, userService) {
		var categorie = this;

		categorie.products = products;
		categorie.title = $stateParams.categorieName;
		categorie.user = $scope.$parent.main.user;
		
		categorie.services = {
			userService : {
				check: userService.checkFavorite,
				set: userService.setFavorite
			}
		};

		function init() {
			
		}

		//INITIALIZING
		init();
	};

	angular
		.module('categorieModule')
			.controller('categorieCtrl', categorieCtrl);
})();
