(function(){
	'use strict';
	angular.module('categorieModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.categorie', {
				url: '/categorias/:categorieName/:categorieId',
				templateUrl: '/scripts/modules/categorie/views/categorie.html',
				controller : 'categorieCtrl as categorie',
				resolve : {
					products : function(productService, $stateParams) {
						var categorieId = $stateParams.categorieId;
						return productService.getProductsByCategorie(categorieId);
						//return productService.retrieveHighlighted()
					}
				}
			});
	});
})();