(function(){

	'use strict';

	var categorieService = function(fireRef, Kutral, $q, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var categorySchema;

		var categoryModel;

		categorySchema = new Kutral.Schema({
			'title': {type: String, indexOn: true}
		});

		categoryModel = kutral.model('categories', categorySchema);

		service.retrieveAll = function(){
			var categoryRetriveAllPromise = $q.defer();

			var ref = new Firebase(fireRef + 'categories');
			categoryRetriveAllPromise.resolve($firebaseArray(ref));

			return categoryRetriveAllPromise.promise;
		};

		service.getCategoryById = function(id){
			var productGetCategoryByIdPromise = $q.defer();
			
			categoryModel.find({$id:id}, function(data){
				productGetCategoryByIdPromise.resolve(data);
			});

			return productGetCategoryByIdPromise.promise;
		};

		service.getCategoryByTitle = function(tit){
			var productGetCategoryByTitlePromise = $q.defer();
			categoryModel.find({title:tit}, function(data){
				productGetCategoryByTitlePromise.resolve(data);
			});

			return productGetCategoryByTitlePromise.promise;
		};
	};
	 
	angular
		.module('categorieModule')
			.service('categorieService', categorieService);

})();