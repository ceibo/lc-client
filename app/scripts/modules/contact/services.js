(function(){

	'use strict';
	
	var contactService = function($q, fireRef, Kutral, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var contactSchema;

		var contactModel;

		contactSchema = new Kutral.Schema({
			'name': {type: String, indexOn: true}
		});

		contactModel = kutral.model('jobs/contact', contactSchema);
		
		service.send = function(data){
			var sendPromise = $q.defer();

			contactModel.data = data;
			contactModel.create(function(success) {
				if(success){
					data = contactModel.data;
				}
				sendPromise.resolve(contactModel.data);
			});
			return sendPromise.promise;
		};
	};
	 
	angular
		.module('contactModule')
			.service('contactService', contactService);

})();