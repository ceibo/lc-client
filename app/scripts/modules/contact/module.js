(function(){
	'use strict';
	angular.module('contactModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.contact', {
				url: '/contacto',
				templateUrl: '/scripts/modules/contact/views/contact.html',
				controller : 'contactCtrl as contact'
			})

			.state('app.lc.contact2', {
				url: '/contacto/:productId',
				templateUrl: '/scripts/modules/contact/views/contact.html',
				controller : 'contactProductCtrl as contact',
				resolve: {
					data : function(productService, $stateParams, $q) {
						var productId = $stateParams.productId;
						var dataPromise = $q.defer();
						productService.getProductById(productId).then(function(firstQueryData){
							productService.getRelatedProducts(firstQueryData.categories[0], firstQueryData.id).then(function(secondQueryData){
								productService.retrieveHighlightedToEntry(productId).then(function(thirdQueryData){
									dataPromise.resolve({productById:firstQueryData, related: secondQueryData, highlighted:thirdQueryData});
								});
							});
						});

						return dataPromise.promise;
					}
				}
			});
	});
})();
