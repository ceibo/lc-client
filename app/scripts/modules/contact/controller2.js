(function(){

    'use strict';
    var contactProductCtrl = function(contactService, data) {
        var contact = this;

        contact.model = {};

        contact.fields = [
            {
                className: 'row',
                fieldGroup: [
                    {
                        className: 'col-sm-4',
                        type: 'input',
                        key: 'name',
                        templateOptions: {
                            type: 'text',
                            label: 'Nombre',
                            required: true,
                            maxlength: 50,
                            minlength: 3,
                        }
                    },
                    {
                        className: 'col-sm-4',
                        type: 'input',
                        key: 'email',
                        templateOptions: {
                            type: 'email',
                            label: 'Email',
                            required: true
                        }
                    },
                    {
                        className: 'col-sm-4',
                        type: 'input',
                        key: 'phone',
                        templateOptions: {
                            type: 'number',
                            label: 'Teléfono',
                            min: 0,
                            minlength: 6,
                            maxlength: 15
                        }
                    },
                    {
                        className: 'col-xs-12',
                        type: 'textarea',
                        key: 'message',
                        defaultValue: "Consulta sobre '" + data.productById.title + "'",
                        templateOptions: {
                            label: 'Mensaje',
                            required: true
                        }
                    }
                ]
            }
        ];

        contact.submit = function(data){
            contact.loading = true;
            contactService.send(data).then(function(res){
                contact.model = {};
                contact.loading = false;
            });
        };

        function init() {

        }

        //INITIALIZING
        init();
    };

    angular
        .module('contactModule')
            .controller('contactProductCtrl', contactProductCtrl);
})();
