(function(){

	'use strict';
	
	var userService = function($q, fireRef, Kutral, $firebaseArray, AuthTokenService, productService) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var userSchema;

		var userModel;

		userSchema = new Kutral.Schema({
			'email': {type: String, indexOn: true}
		});

		userModel = kutral.model('users', userSchema);

		service.getUserById = function(id){
			var productGetProductByIdPromise = $q.defer();

				userModel.find({$id:id}, function(data) {
					productGetProductByIdPromise.resolve(data);
				});

			return productGetProductByIdPromise.promise;
		};
		
		service.setInformation = function(info, user){
			var setInformationPromise = $q.defer();

			service.getUserById(user.uid).then(function(data){
				$.extend(data, info);
				userModel.data = data;
				userModel.update(function(success) {
					if(success){
						data = userModel.data;
						AuthTokenService.setUser(data);

					}
					setInformationPromise.resolve(userModel.data);
				});
			});

			return setInformationPromise.promise;
		};

		service.updateAvatar = function(photoUrl, photoId, uid){
			var updateAvatarPromise = $q.defer();

			service.getUserById(uid).then(function(data){
				data.photoUrl = photoUrl;
				data.photoId = photoId;
				userModel.data = data;
				userModel.update(function(success) {
					if(success){
						data = userModel.data;
						AuthTokenService.setUser(data);

					}
					updateAvatarPromise.resolve(userModel.data);
				});
			});

			return updateAvatarPromise.promise;
		};

		service.retriveFavorites = function(uid, favoritesId){
			var retriveFavoritesPromise = $q.defer(),
				promisesArrayFavorites = [];

			if(favoritesId){
				favoritesId.forEach(function(id){
					var loopPromise = $q.defer();
					productService.getProductById(id).then(function(data){
						if(data){
							loopPromise.resolve(data);
						}else{
							loopPromise.resolve();
						}
					});
					promisesArrayFavorites.push(loopPromise.promise);
				});
			}else{
				retriveFavoritesPromise.resolve();
			}

			$q.all(promisesArrayFavorites).then(function(results) {
				
				for (var i in results) {
				  if (results[i] === null || results[i] === undefined) {
				  // test[i] === undefined is probably not very useful here
				    delete results[i];
				  }
				}
				retriveFavoritesPromise.resolve(results);
			});

			return retriveFavoritesPromise.promise;
		};

		service.checkFavorite = function(user, productId){
			var checkFavoritePromise = $q.defer(),
				favoriteFinded;
			if(user && productId){
				if(user.favorites){
					user.favorites.forEach(function(favorite){
						if(favorite == productId){
							favoriteFinded = true;
							return;
						}
					});

					if(favoriteFinded){
						checkFavoritePromise.resolve(favoriteFinded);
					}else{
						checkFavoritePromise.resolve();
					}
				}else{
					checkFavoritePromise.resolve();
				}

			}else{
				checkFavoritePromise.resolve();
			}

			return checkFavoritePromise.promise;	
		};

		service.setFavorite = function(uid, productId, exist){
			var setFavoritePromise = $q.defer();
			if(uid && productId){
				service.getUserById(uid).then(function(data){
					if(!data.favorites){
						data.favorites = [];
					}
					if(!exist){
						//add to favorites array
						data.favorites.push(productId);
						//Remove duplicate from array
						data.favorites = data.favorites.filter(function(item, pos, self) {
							return self.indexOf(item) == pos;
						});
					}else{
						//remove from favorites array
						var i = data.favorites.indexOf(productId);
						if(i != -1) {
							data.favorites.splice(i, 1);
						}
					}
					userModel.data = data;
					userModel.update(function(success) {
						if(success){

						}
						setFavoritePromise.resolve(userModel.data);
					});
				});
			}else{
				setFavoritePromise.resolve();
			}

			return setFavoritePromise.promise;	
		};

	};
	 
	angular
		.module('userModule')
			.service('userService', userService);

})();