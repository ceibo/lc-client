(function(){
	'use strict';
	angular.module('userModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.user', {
				url: '/cuenta',
				templateUrl: '/scripts/modules/user/views/account.html',
				controller : 'userCtrl as user',
				resolve: {
					userData: function(AuthTokenService, $state, userService){
						return AuthTokenService.getUserData().then(function(data){
							if(data){
								if(!data.uid){data.uid = data.id;}
								return userService.getUserById(data.uid).then(function(userData){
									if(!userData.favorites){
										userData.favorites = [];
									}
									return userService.retriveFavorites(data.uid, userData.favorites).then(function(favorites){
										return {'data': userData, 'uid': data.uid, 'favorites' : favorites};
									});
								});
							}else{
								$state.go('app.lc.home');
							}
						});
					}
				}
			});
	})

	.directive('googleplace', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, model) {
				var options = {
					types: [],
					componentRestrictions: {}
				};
				scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

				google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
					scope.$apply(function() {
						model.$setViewValue(element.val());
					});
				});
			}
		};
	});


})();