(function(){

	'use strict';
	var userCtrl = function($scope, userService, userData, AuthTokenService, messageBoxService) {
		var user = this;
		
		user.model = userData.data;
		user.guns = userData;
		user.credentials = {};
		user.userData = userData.data;
		user.uid = userData.uid;
		user.favorites = userData.favorites;

		user.services = {
			userService : {
				check: userService.checkFavorite,
				set: userService.setFavorite
			}
		};

		user.fields = [
			{
				className: 'row',
				fieldGroup: [
					{
						className: 'col-xs-12 col-md-6',
						type: 'input',
						key: 'firstName',
						templateOptions: {
							type: 'text',
							label: 'Nombre',
							maxlength: 20,
							minlength: 0,
							required: true
						}
					},
					{
						className: 'col-xs-12 col-md-6',
						type: 'input',
						key: 'lastName',
						templateOptions: {
							type: 'text',
							label: 'Apellido',
							maxlength: 20,
							minlength: 0,
							required: true
						}
					},
					{
						className: 'col-xs-12 col-md-6',
						type: 'input',
						key: 'dni',
						templateOptions: {
							type: 'text',
							label: 'DNI',
							pattern: '\\d{7,8}',
							required: true
						}
					},
					{
						className: 'col-xs-6 col-md-3',
						type: 'input',
						key: 'cuil',
						templateOptions: {
							type: 'text',
							label: 'CUIL',
							// pattern: '\\d{1,2}-\\d{7,8}-\\d{1,2}',
							placeholder: '55-55555555-55',
							required: true
						}
					},
					{
						className: 'col-xs-6 col-md-3',
						type: 'input',
						key: 'phone',
						templateOptions: {
							type: 'number',
							label: 'Teléfono',
							min: 0,
							required: true
						}
					},
					{
						className: 'col-xs-12 col-md-6',
						type: 'input',
						key: 'city',
						ngModelAttrs: {
							"": {
								"value": "googleplace"
							}
						},
						templateOptions: {
							type: 'text',
							label: 'Ciudad',
							required: true
						}						
					},
					{
						className: 'col-xs-6 col-md-3',
						type: 'input',
						key: 'street',
						templateOptions: {
							type: 'text',
							label: 'Domicilio',
							required: true
						}
					},
					{
						className: 'col-xs-6 col-md-3',
						type: 'input',
						key: 'zip',
						templateOptions: {
							type: 'number',
							label: 'Código postal',
							max: 99999,
							min: 0,
							pattern: '\\d{4}',
							required: true
						}
					}
				]
			}
		];

		user.fieldsGuns = [
			{
				className: 'row',
				fieldGroup: [
					{
						className: 'col-xs-12 col-md-6',
						type: 'input',
						key: 'cluaucc',
						templateOptions: {
							type: 'number',
							label: 'CLUAUCC',
							minlength: 0,
						}
					},
					{
						className: 'col-xs-12 col-md-6',
						type: 'input',
						key: 'cluauc',
						templateOptions: {
							type: 'number',
							label: 'CLUAUC',
							minlength: 0,
						}
					},
					{
						className: 'col-xs-12',
						type: 'input',
						key: 'docket',
						templateOptions: {
							type: 'text',
							label: 'Legajo',
							pattern: '3-\\d{1,2}.\\d{3}.\\d{3}',
							placeholder: '3-55.555.55',
							minlength: 0,
							required: true
						},
						hideExpression: '!model.cluaucc && !model.cluauc'
					},
					{
						className: 'col-xs-12 col-md-6',
						type: 'input',
						key: 'consumerCard',
						templateOptions: {
							type: 'number',
							label: 'Tarjeta de consumo',
							required: true,
							minlength: 0,
						}
					},
					{
						className: 'col-xs-12 col-md-6',
						type: 'input',
						key: 'holding',
						templateOptions: {
							type: 'number',
							label: 'Tenencia',
							required: true,
							minlength: 0,
						}
					}
				]
			}
		];

		user.fieldsCredentials = [
			{
				className: 'row',
				fieldGroup: [
					{
						className: 'col-xs-12',
						type: 'input',
						key: 'email',
						templateOptions: {
							type: 'email',
							label: 'Email',
							required: true
						}
					},
					{
						className: 'col-xs-12',
						type: 'input',
						key: 'oldPassword',
						templateOptions: {
							type: 'text',
							label: 'Contraseña Actual',
							required: true
						}
					},
					{
						className: 'col-xs-12',
						type: 'input',
						key: 'newPassword',
						templateOptions: {
							type: 'text',
							label: 'Nueva contraseña',
							required: true
						}
					}
				]
			}
		];

		user.setInformation = function(info, usr){
			user.loading = true;
			info.username = info.firstName + ' ' + info.lastName || info.email;
			userService.setInformation(info, usr).then(function(data){
				if(data){
					user.updateMainUser(data);
					user.loading = false;
					messageBoxService.showSuccess('Información actualizada.');
				}
			});
		};

		/*user.updateMainUser = function(data){
			$scope.$parent.main.user = Object.assign(user.userData, data);
		}*/

		user.updateMainUser = function(data){
			for (var attr in data) { user.userData[attr] = data[attr]; }
			$scope.$parent.main.user = user.userData;
		};

		user.setInformationGuns = function(info, user){
			user.loadingGuns = true;
			userService.setInformation(info, user).then(function(data){
				if(data){
					user.loadingGuns = false;
					messageBoxService.showSuccess('Información actualizada.');
				}
			});
		};

		user.changePassword = function(userCredentials, userData) {
			user.loadingCredentials = true;

	        user.changePromise = AuthTokenService.changePassword(userCredentials)
	            .then(function() {
	                user.loadingCredentials = false;
	                messageBoxService.showSuccess('Contraseña actualizada.');
	            }, function(error) {
	            	user.loadingCredentials = false;
	                if(error == 'Error: The specified password is incorrect.'){
	                	user.error = 'La contraseña actual es incorrecta'
	                }
	                if(error == 'Error: The specified user does not exist.'){
	                	user.error = 'El correo electrónico es incorrecto'
	                }

	                messageBoxService.showError(user.error);
	            });
		};

		function init() {
			user.updateMainUser($scope.$parent.main.user);
		}

		//INITIALIZING
		init();
	};

	angular
		.module('userModule')
			.controller('userCtrl', userCtrl);
})();
