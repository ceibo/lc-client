(function(){

	'use strict';

	var productService = function(fireRef, Kutral, $q, $firebaseArray, $firebaseObject) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var productSchema;

		var productModel;
		var publishedProducts;
		var searchProducts = null;

		var instantiateModels = function() {
			var instantiateModelsPromise = $q.defer();

			productSchema = new Kutral.Schema({
				'title': {type: String, indexOn: true},
				'author': {type: 'ObjectId', ref:'users', indexOn: true},
				'categories': {type: 'ObjectId', ref:'categories'},
				'subcategories': {type: 'ObjectId', ref:'subcategories'},
				'brand': {type: 'ObjectId', ref:'brands'}
			});

			productModel = kutral.model('products', productSchema);
			publishedProducts = kutral.model.discriminator('published', productModel);
			instantiateModelsPromise.resolve();

			return instantiateModelsPromise.promise;
		};

		service.retrieveHighlighted = function(){
			var productRetriveHighlightedPromise = $q.defer();

			instantiateModels().then(function() {
				publishedProducts.find({highlight:true}).limit(4).$asArray(function(data){
					productRetriveHighlightedPromise.resolve(data);
				});
			});
			return productRetriveHighlightedPromise.promise;
		};

		service.retrieveHighlightedToEntry = function(productId){
			var productRetrieveHighlightedToentryPromise = $q.defer();
			var promisesArrayCategories = [];
			var loopStop = false;
			instantiateModels().then(function() {
				publishedProducts.find({highlight:true}).limit(3).$asArray(function(data){
					if(data.length >= 2){
						data.forEach(function(product) {
							if(product.$id !== productId){
								if(promisesArrayCategories.length === 2){ loopStop = true; }
								if(loopStop){ return; }
								var loopPromise = $q.defer();
								loopPromise.resolve(product);
								promisesArrayCategories.push(loopPromise.promise);
							}
						});
						$q.all(promisesArrayCategories).then(function(results) {
							productRetrieveHighlightedToentryPromise.resolve(results);
						});
					}else{
						productRetrieveHighlightedToentryPromise.resolve(data);
					}
				});
			});
			return productRetrieveHighlightedToentryPromise.promise;
		};

		service.retrieveSlider = function(){
			var productRetriveSliderPromise = $q.defer();
			instantiateModels().then(function() {
				publishedProducts.find({slider:true}).$asArray(function(data){
					productRetriveSliderPromise.resolve(data);
				});
			});
			return productRetriveSliderPromise.promise;
		};

		service.getProductById = function(id){
			var getProductByIdPromise = $q.defer();

			var ref = $firebaseObject(new Firebase(fireRef + 'products/published/' + id));
			ref.$loaded().then(function(data) {
				if(data.$value === undefined){
					data.$id = ref.$ref().key();
					data.id = data.$id;
					if(data.$$conf) {
						delete data.$$conf;
					}
					getProductByIdPromise.resolve(data);
				}else{
					getProductByIdPromise.resolve();
				}
			});

			return getProductByIdPromise.promise;
		};

		service.getProductsCart = function(id){
			var getProductByIdPromise = $q.defer();
			instantiateModels().then(function() {
				publishedProducts.find({$id:id}, function(data){
					getProductByIdPromise.resolve(data);
				});
			});
			return getProductByIdPromise.promise;
		};

		service.searchProducts = function(title, selectedCategorie){
			var searchProductsPromise = $q.defer();
			instantiateModels().then(function() {
				var badchars = '/\@:?%!&£$#~<>^*+=";(){}[]', badcharFinded;
				for (var x=0; x < title.length; x++) {
					if (badchars.indexOf(title.charAt(x)) !== -1) {
						badcharFinded = true;
						break;
					}
					if(badcharFinded){
						break;
					}
				}

				if(!badcharFinded) {
					publishedProducts.match({$and: [{'title':title.toString()}]}, function(data) {
						for (var key in data) {
							if (data.hasOwnProperty(key)) {
								data[key].$id = key;
							}
						}

						searchProductsPromise.resolve(resultsToArray(data));
					});
				}else{
					searchProductsPromise.reject();
				}
			});
			return searchProductsPromise.promise;
		};

		service.getProductsByCategorie = function(id){
			var getProductsByCategoriePromise = $q.defer();
			instantiateModels().then(function() {
				publishedProducts.find({'$and' : [{ categories : { '$in': [id]} }] }).$asArray(function(data) {
					getProductsByCategoriePromise.resolve(data);
				});

			});
			return getProductsByCategoriePromise.promise;
		};

		service.getProductsBySubCategory = function(categoryId, subcategoryId){
			var getProductsBySubCategoryPromise = $q.defer();
			instantiateModels().then(function() {
				publishedProducts.find({'$and' : [{ categories : { '$in': [categoryId]} , subcategories : { '$in': [subcategoryId]} }  ] }).$asArray(function(data) {
					getProductsBySubCategoryPromise.resolve(data);
				});

			});
			return getProductsBySubCategoryPromise.promise;
		};

		service.getProductsByBrand = function(id){
			var getProductsByBrandPromise = $q.defer();
			instantiateModels().then(function() {
				publishedProducts.find({ brand : id }).$asArray(function(data) {
					getProductsByBrandPromise.resolve(data);
				});

			});
			return getProductsByBrandPromise.promise;
		};

		service.getRelatedProducts = function(categorieId, productId){
			var getRelatedProductsPromise = $q.defer();
			var promisesArrayCategories = [];
			var loopStop = false;
			instantiateModels().then(function() {
				publishedProducts.find({'$and' : [{ categories : { '$in': [categorieId]} }] }).limit(5).$asArray(function(data) {
					//if(data.length >= 4){
						data.forEach(function(product) {
							if(product.$id !== productId){
								if(promisesArrayCategories.length === 4){ loopStop = true; }
								if(loopStop){ return; }
								var loopPromise = $q.defer();
								loopPromise.resolve(product);
								promisesArrayCategories.push(loopPromise.promise);
							}
						});

						$q.all(promisesArrayCategories).then(function(results) {
							getRelatedProductsPromise.resolve(results);
						});
				});
			});
			return getRelatedProductsPromise.promise;
		};

		service.setSearchProducts = function(p){
			searchProducts = p;
		};

		service.getSearchProducts = function(){
			return searchProducts;
		};
	};

	var resultsToArray = function(data) {
		var results = [];
		if(data) {
			_.map(data, function(element) {
				results.push(element);
			});
		}

		return results;
	};

	angular
		.module('productModule')
			.service('productService', productService);

})();