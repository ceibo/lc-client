(function(){

	'use strict';
	var productCtrl = function($scope, data, userService, ngDialog) {
		var product = this;

		product.services = {
			userService : {
				check: userService.checkFavorite,
				set: userService.setFavorite
			}
		};

		product.user = $scope.$parent.main.user;

	    product.popupImage = function (selectedProduct) {
	    	// open a modal dialog with an larger image 
	    	if (!selectedProduct.photoUrl) {
	    		// if no url, don't open the dialog
	    		return false;
	    	}
	        ngDialog.open({
			    template: 'scripts/modules/product/views/product_dialog.html',
			    controller: function () {
			    	this.photoUrl = selectedProduct.photoUrl;
			    	this.title = selectedProduct.title;
			    },
			    controllerAs: 'dialog'
			});
	    };

		function init() {
			product.item = data.productById;
			product.related = data.related;
			product.highlighted = data.highlighted;
		}

		//INITIALIZING
		init();
	};

	angular
		.module('productModule')
			.controller('productCtrl', productCtrl);
})();
