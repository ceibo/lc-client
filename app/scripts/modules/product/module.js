(function(){
	'use strict';
	angular.module('productModule', [])

	.config(function ($stateProvider) {

		$stateProvider
			.state('app.lc.product', {
				url: '/producto/:productId',
				templateUrl: '/scripts/modules/product/views/product.html',
				controller : 'productCtrl as product',
				resolve: {
					data : function(productService, $stateParams, $q) {
						var productId = $stateParams.productId;
						var dataPromise = $q.defer();
						productService.getProductById(productId).then(function(firstQueryData){
							productService.getRelatedProducts(firstQueryData.categories[0], firstQueryData.id).then(function(secondQueryData){
								productService.retrieveHighlightedToEntry(productId).then(function(thirdQueryData){
									dataPromise.resolve({productById:firstQueryData, related: secondQueryData, highlighted:thirdQueryData});
								});
							});
						});

						return dataPromise.promise;
					}
				}
			});
	})

	.directive('lcProducts', function() {
		return {
			restrict: 'E',
			transclude: true,
			templateUrl: 'template/products.html',
			scope: {products: '=', topTitle: '@', itemsPerPage: '@', user: '=', services: '='}
		};
	})

	.directive('lcProduct', function() {
		return {
			restrict: 'E',
			transclude: true,
			templateUrl: 'template/product.html',
			scope: {product: '=', user: '=', services: '=', popupImageFunction: '='}
		};
	})

	.directive('lcRelated', function() {
		return {
			restrict: 'E',
			transclude: true,
			templateUrl: 'template/related.html',
			scope: {products: '='}
		};
	})

	.directive('lcHighlighted', function() {
		return {
			restrict: 'E',
			transclude: true,
			templateUrl: 'template/highlighted.html',
			scope: {products: '=', user: '=', services: '='}
		};
	});

})();
