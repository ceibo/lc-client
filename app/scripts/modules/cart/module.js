(function(){
	'use strict';
	angular.module('cartModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.cart', {
				url: '/carrito',
				templateUrl: '/scripts/modules/cart/views/cart.html',
				controller: 'cartCtrl as cart',
				resolve: {
					items : function(cartService) {
						return cartService.getItems().then(function(results){
							return results;
						});
					},
					userData: function(AuthTokenService, userService){
						return AuthTokenService.getUserData().then(function(data){
							if(data){
								if(!data.uid){data.uid = data.id;}
								return userService.getUserById(data.uid).then(function(userData){
									return userData;
								});
							}
						});
					}
				}
			});
	});
})();