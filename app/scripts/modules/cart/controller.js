(function(){

	'use strict';
	var cartCtrl = function(items, ngCart, ngCartItem, cartService, $state, AuthTokenService, userService, userData) {
		var cart = this;
		cart.checkoutSuccess = null;

		cart.getQuantity = function(){
			cart.items.forEach(function(item) {
				if(item){
					item.quantity = ngCart.getItemById(item.id)._quantity;
				}
			});
		};

		cart.setQuantity = function(item, quantity){
			if(quantity == -1){
					var item = ngCart.getItemById(item.id);
					item.setQuantity(quantity, true);
					cart.getQuantity();
			}else{
				if(item.quantity !== item.stock){
					var item = ngCart.getItemById(item.id);
					item.setQuantity(quantity, true);
					cart.getQuantity();
				}
			}
		};

		cart.removeItemById = function(id, collection, product){
			ngCart.removeItemById(id);
			cartService.removeElement(collection, product);
		};

		cart.getTax = function(){
			return ngCart.getTax();
		};

		cart.getTaxRate = function(){
			return ngCart.getTaxRate();
		};

		cart.getShipping = function(){
			return ngCart.getShipping();
		};

		cart.getItemTotal = function(quantity, price){
			var total = 0;
			total = quantity * price;
			return total;
		};

		cart.getTotal = function(){
			var total = 0;
			cart.items.forEach(function(item) {
				total += cart.getItemTotal(item.quantity, item.price);
			});

			total += cart.getTax() + cart.getShipping();

			return total;
		};

		cart.checkout = function(items){
			cart.loading = true;
			userService.getUserById(userData.uid).then(function(userData){
				if(userData){
					var sale = [];
					sale.push(items, userData);
					
					cartService.pushSale(sale).then(function(data){
						ngCart.empty(true);
						cart.checkoutSuccess = true;
						cart.loading = false;
					});
				}
			});
		};

		cart.returnToHome = function(){
			$state.go('app.lc.home');
		};

		function init() {
			cart.items = items;
			cart.getQuantity();
			if(userData){
				if(!userData.uid){userData.uid = userData.id;}
				cart.user = userData;
				if(!cart.user.firstName || !cart.user.lastName || !cart.user.city || !cart.user.street || !cart.user.zip){
					cart.error = true;
				}
				/*if(!cart.user.cluaucc || !cart.user.docket || !cart.user.consumerCard || !cart.user.tendency){
					cart.gunsError = true;
				}*/
			}else{
				cart.user = null;
			}

			/*cart.items.forEach(function(item) {
				if(item){
					for (var i = 0; i < item.categories.length; i++) {
						if(item.categories[i] == '-JtAhzOLZluhhAgmUdz2'){
							cart.guns = true;
						}
					};
				}
			})*/
		}

		//INITIALIZING
		init();
	};

	angular
		.module('lascoloradasApp')
			.controller('cartCtrl', cartCtrl);
})();
