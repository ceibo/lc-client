(function(){

	'use strict';
	

	var cartService = function(ngCart, productService, $q, fireRef, Kutral, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var saleSchema;

		var saleModel;

		saleSchema = new Kutral.Schema({
			'name': {type: String, indexOn: true}
		});

		saleModel = kutral.model('jobs/sales', saleSchema);

		service.getItems = function() {
			var getItemsPromise = $q.defer();
			var promisesArray = [];
			var items = ngCart.getItems();
			items.forEach(function(item) {
				var loopPromise = $q.defer();
				productService.getProductsCart(item._data.$id).then(function(data) {
					if(!data){
						ngCart.removeItemById(item._data.$id);
					}
					loopPromise.resolve(data);
				});

				promisesArray.push(loopPromise.promise);
			});

			
			$q.all(promisesArray).then(function(results) {
				var resultNotNull = [];
				for (var i = 0; i < results.length; i++) {
					if(results[i]){
						resultNotNull.push(results[i]);
					}
				}
				getItemsPromise.resolve(resultNotNull);
			});
			
			return getItemsPromise.promise;
		};

		service.removeElement = function(collection, element) {
			var index = collection.indexOf(element);
			collection.splice(index, 1);
		};

		service.pushSale = function(sale, userData){
			var pushSalePromise = $q.defer();

			saleModel.data = sale;
			saleModel.create(function(success) {
				if(success){
					sale = saleModel.data;
				}
				pushSalePromise.resolve(saleModel.data);
			});
			return pushSalePromise.promise;
		};

	};
	 
	angular
		.module('cartModule')
			.service('cartService', cartService);

})();