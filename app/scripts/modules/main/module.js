(function(){
	'use strict';
	angular.module('mainModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');

		$stateProvider
			.state('app', {
				abstract: true,
				templateUrl: 'scripts/modules/main/views/main.html',
				controller : 'mainCtrl as main',
				resolve: {
					categories : function(categorieService) {
						return categorieService.retrieveAll();
					},
					brands : function(brandsService) {
						return brandsService.getBrandsHighlighted();
					},
					userData: function(AuthTokenService, $state, userService){
						return AuthTokenService.getUserData().then(function(data){
							if(data){
								if(!data.uid){data.uid = data.id;}
								return userService.getUserById(data.uid).then(function(userData){
									return {'data': userData, 'uid': data.uid};
								});
							}else{
								return {};
							}
						});
					}
				}
			})

			.state('app.lc', {
				abstract: true,
				template:'<div ui-view=""></div>'
			});
	})

	.directive('lcHeader', function() {
		return {
			restrict: 'E',
			transclude: true,
			controller: 'navBarCtrl as nav',
			scope: {categories: '=', user: '='},
			templateUrl: 'template/header.html',
			link: function(scope) {
				scope.selectedCategorie = scope.categories[0];
			}
		};
	})

	.directive('lcFooter', function() {
		return {
			restrict: 'E',
			transclude: true,
			controller: 'footerCtrl as footer',
			templateUrl: 'template/footer.html',
			scope: {brands: '='}
		};
	});
})();
