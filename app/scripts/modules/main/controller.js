(function(){

	'use strict';
	var mainCtrl = function(categories,brands, userData, $state) {
		var main = this;
		main.user = [];

		if(userData.data || userData.uid){
			if(userData.data){
				main.user = userData.data;
			}
			main.user.uid = userData.uid;
			main.user.username = main.user.username || main.user.firstName || main.user.email;
		}
		//main.userData = userData.data;
		main.state = $state;

		function init() {
			main.categories = categories;
			main.brands = brands;
		}

		//INITIALIZING
		init();
	};

	var navBarCtrl = function($state, $rootScope, ngCart, ngCartItem, productService, AuthTokenService, subcategoriesService, messageBoxService) {

		var nav = this;

		nav.subcategoriesService = {
			get: subcategoriesService.getSubcategoryById
		};

		nav.getItems = function(){
			return ngCart.getTotalItems();
		};

		nav.search = function(title, selectedCategorie){
			productService.searchProducts(title, selectedCategorie).then(function(data){
				productService.setSearchProducts(data);
				if($rootScope.$state.current.name !== 'app.lc.search'){
					$state.go('app.lc.search');
				}else{
					$state.go($state.current, {}, {reload: true});
				}
			}, function(err){
				messageBoxService.showError('Se ingresaron caracteres inválidos.');
			});
		};

		nav.logOut = function(){
			AuthTokenService.logOut().then(function(){
				$state.go('app.lc.home', {}, {reload: true});
			});
		};

		function init() {
			//ngCart.empty(true)
			$rootScope.$on('ngCart:change',function(res){
				nav.getItems();
			});
		}

		init();
	};

	var footerCtrl = function(brandsService) {
		var footer = this;

		footer.brands = brandsService.getBrandsHighlighted().then(function(data) {
			footer.brands = data;
		});

	};

	angular
		.module('mainModule')
			.controller('mainCtrl', mainCtrl)
			.controller('navBarCtrl', navBarCtrl)
			.controller('footerCtrl', footerCtrl);
})();
