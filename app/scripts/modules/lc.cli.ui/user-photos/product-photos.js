'use strict';

var userPhotoCtrl = function(uploadSettings, Upload, $timeout) {
    var photoCtrl = this;
    var _uploadSettings = uploadSettings.getSettings('upload');

    photoCtrl.progress = 0;

    photoCtrl.validate = function($file) {
        if ($file) {
            try {
                if (_uploadSettings.allowedType.indexOf($file.type) === -1) {
                    throw new Error(gettext('Only JPG, PNG, GIF Files are allowed'));
                }
                if ($file.size > _uploadSettings.maxSize) {
                    throw new Error(gettext('File must not exceed 2Mb'));
                }
                return $file;
            } catch (error) {
                photoCtrl.error = error.message;
            }

        }
    };

    photoCtrl.deleteFile = function(photoId){
        uploadSettings.deleteFile(photoId).then(function(data){
            if(photoCtrl.photoSliderId == data){
                photoCtrl.photoSliderUrl = '';
                photoCtrl.photoSliderId = null;
            }else{
                photoCtrl.photoUrl = '';
                photoCtrl.photoId = null;
            }
        });
    };

    photoCtrl.upload = function() {
        if (photoCtrl.file[0]) {
            var uploadParams = {
                upload_preset: _uploadSettings.presets.upload
            };
            var oldPhotoId = photoCtrl.photoId;
            var oldPhotoUrl = photoCtrl.photoUrl;
            photoCtrl.progress = 0;
            photoCtrl.uploadPromise = Upload.upload({
                url: _uploadSettings.url,
                method: 'POST',
                fields: uploadParams,
                file: photoCtrl.file[0]
            }).success(function(data) {
                photoCtrl.photoUrl = data.url;
                photoCtrl.photoId = data.public_id;
                if (oldPhotoId && oldPhotoUrl) {
                    uploadSettings.deleteFile(oldPhotoId);
                }
                $timeout(function(){
                	photoCtrl.progress = 0;
                }, 5000);

            }).error(function(err) {
                photoCtrl.error = err.error.message;
            }).progress(function(evt) {
                // Math.min is to fix IE which reports 200% sometimes
                photoCtrl.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };
};

angular.module('lc.cli.ui.photo-uploader', [])
    .controller('userPhotoCtrl', userPhotoCtrl)
    .directive('userPhoto', function(uploadSettings, Upload) {
        // Runs during compile
        return {
            scope: {
                photoUrl: '=',
                photoId: '=',
                userId: '='
            }, // {} = isolate, true = child, false/undefined = no change
            controller: 'userPhotoCtrl as photoUploader',
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: '../scripts/modules/lc.cli.ui/user-photos/user-photos.html',
            bindToController: true
        };
    });