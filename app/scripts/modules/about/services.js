(function(){

	'use strict';
	
	var aboutService = function(fireRef, Kutral, $q, $firebaseArray) {
		var service = this;

		var kutral = new Kutral(fireRef);
		var aboutSchema;

		var aboutModel;

		aboutSchema = new Kutral.Schema({
			
		});

		aboutModel = kutral.model('about', aboutSchema);

		service.retrieveAll = function(){
			var aboutRetriveAllPromise = $q.defer();

			aboutModel.find().$asArray(function(data){
				aboutRetriveAllPromise.resolve(data[0]);
			});

			return aboutRetriveAllPromise.promise;
		};
	};
	 
	angular
		.module('aboutModule')
			.service('aboutService', aboutService);

})();