(function(){

	'use strict';
	var aboutCtrl = function(data, $sce) {
		var about = this;
		about.images = data.images;
		about.description = $sce.trustAsHtml("" + data.description + "");

		function init() {
			$('.scroller').perfectScrollbar();
		}

		//INITIALIZING
		init();
	};

	angular
		.module('aboutModule')
			.controller('aboutCtrl', aboutCtrl);
})();
