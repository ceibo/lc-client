(function(){
	'use strict';
	angular.module('aboutModule', [])

	.config(function ($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('app.lc.about', {
				url: '/sobre-nosotros',
				templateUrl: '/scripts/modules/about/views/about.html',
				controller : 'aboutCtrl as about',
				resolve : {
					data : function(aboutService) {
						return aboutService.retrieveAll();
					}
				}
			});
	})

	.directive('lcAboutSlider', function() {
		return {
			restrict: 'E',
			transclude: true,
			scope: {images: '='},
			templateUrl: 'template/about-slider.html',
			link: function(scope, element, attrs, controllers) {
				
			}
		};
	});
})();