angular.module('auth',['fireServiceModule'])
 .config(['$stateProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('app.lc.auth', {
        abstract:true,
        url:'/acceso',
        template:'<div ui-view=""></div>'
      })

      .state('app.lc.auth.login',{
        url: '/entrar',
        templateUrl: '/scripts/modules/auth/views/login.html',
        controller: 'authCtrl as login'
      })

      .state('app.lc.auth.signup', {
        url: '/signup',
        templateUrl: 'views/signup.html',
        controller: 'authCtrl as authCtrl'
      })

      .state('app.lc.auth.password', {
        url: '/password',
        templateUrl: 'views/password.html',
        controller: 'authCtrl as authCtrl'
      })

      .state('app.lc.auth.register', {
        url: '/registrarse',
        templateUrl: '/scripts/modules/auth/views/register.html',
        controller: 'authCtrl as register'
      });
  }]);
