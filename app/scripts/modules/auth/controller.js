/**
 * Created by emiliano on 04/02/15.
 */

var authCtrl = function($state, AuthTokenService, $localStorage, messageBoxService, userService) {

    var authCtrl = this;

    authCtrl.optin = {
        email: '',
        captcha: ''
    };
    authCtrl.flash = $localStorage.flash;

    if(authCtrl.flash){
        delete $localStorage.flash;
    }

    authCtrl.login = function(userCredentials) {
        authCtrl.loading = true;
        authCtrl.loginPromise = AuthTokenService.login(userCredentials)
            .then(function(data) {
                AuthTokenService.getUserData(data)
                    .then(function(userData) {
                        if(!userData.uid){userData.uid = userData.id}
                        userService.getUserById(userData.uid).then(function(user){
                            $state.go('app.lc.home', {}, { reload: true });
                            authCtrl.loading = false;
                            var username = user.firstName || user.email;
                            messageBoxService.showSuccess('Bienvenido, '+ username +'.');
                        })
                    });
            }, function(error) {
                authCtrl.loading = false;
                if(error.code.indexOf('INVALID_') !== -1){
                    //authCtrl.error = 'Email o Password incorrectos';
                    messageBoxService.showError('Email o Password incorrectos.');
                }
                else{
                    // $raven.captureException(error);
                    //authCtrl.error = 'Nuestra tienda esta presentando problemas de funcionamiento, disculpe las molestias.';
                    messageBoxService.showError('Nuestra tienda esta presentando problemas de funcionamiento, disculpe las molestias.');
                }
            });
    };

    authCtrl.toggleFotgonPass = function(){
        authCtrl.forgot = !authCtrl.forgot;
    }

    authCtrl.optinUser = function(optin) {
        optin.culture = $localStorage.locale || 'es';
        authCtrl.loading = true;
        AuthTokenService.optin(optin)
            .then(function() {
                //authCtrl.signUpSuccess = 'Registro completo, por favor, siga las instrucciones enviadas a su dirección de correo electrónico';
                messageBoxService.showSuccess('Registro completo, por favor, siga las instrucciones enviadas a su dirección de correo electrónico');
                authCtrl.loading = false;
            }, function(error) {
                //authCtrl.error = error;
                messageBoxService.showError(error);
                authCtrl.loading = false;
            });
    };

    authCtrl.checkUserNameAvailability = function(username) {
        if (username) {
            authCtrl.checkingAvailability = true;
            AuthTokenService.checkUserNameAvailability(username)
                .then(function(availability) {
                    authCtrl.userNameAvailable = availability;
                    authCtrl.checkingAvailability = false;
                })
        }
    }

    authCtrl.resetCredentials = function(userCredentials) {
        var email = {email: userCredentials}
        authCtrl.resetPromise = AuthTokenService.reset(email)
            .then(function() {
                //$state.go('auth.login');
                authCtrl.resetSuccess = 'Una nueva contraseña se ha enviado a su dirección de correo electrónico'
                $localStorage.flash = 'Una nueva contraseña se ha enviado a su dirección de correo electrónico';
            }, function(error) {
                authCtrl.error = error;
            });
    };

    authCtrl.changePassword = function(userCredentials) {
        var email = {email: userCredentials}
        authCtrl.changePromise = AuthTokenService.changePassword(email)
            .then(function() {
                console.log('aca')
            }, function(error) {
                authCtrl.error = error;
            });
    };

    authCtrl.resetRepeatedPass = function(pass) {
        if (!pass) {
            authCtrl.user.repeat = ""
        }
    }
};

angular.module('auth')
    .controller('authCtrl', authCtrl);
