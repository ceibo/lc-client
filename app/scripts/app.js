(function () {

  'use strict';

  angular
    .module('lascoloradasApp', [
      'envConfig',
      'ceibo.ui',
      'ui.router',
      'ngAnimate',
      'mgcrea.ngStrap',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngStorage',
      'ngSanitize',
      'formly',
      'formlyBootstrap',
      'angularUtils.directives.dirPagination',
      'ngMessages',
      'angularFileUpload',
      'ngFileUpload',
      'ngDialog',
      'angularMoment',
      'auth',
      'utils',
      'ngMap',
      'ngCart',
      'ngTouch',
      'fireQuery',
      'logglyLogger',
      'mainModule',
      'homeModule',
      'cartModule',
      'userModule',
      'aboutModule',
      'searchModule',
      'contactModule',
      'productModule',
      'categorieModule',
      'subcategoryModule',
      'brandsModule',
      //UI
      'lc.cli.ui.photo-uploader',
      'lc.components.subcategories.displayer',
      'lc.components.favorites.add-to-favorite'
    ])

    .constant('LoggyToken', 'fed68fd5-bcea-4a61-92ac-1d32c4ad01db')

    .run(function ($state, $rootScope, anchorSmoothScroll, messageBoxService, formlyConfig, formlyValidationMessages) {
      $rootScope.$state = $state;

      //ngCart.empty();

      $rootScope.$on('ngCart:itemRemoved',function(){
        //messageBoxService.showInfo('Item eliminado satisfactoriamente');
      });

      $rootScope.$on('ngCart:itemAdded',function(){
        messageBoxService.showSuccess('Producto añadido satisfactoriamente');
      });

      $rootScope.$on('$stateChangeSuccess', function() {
        //document.body.scrollTop = document.documentElement.scrollTop = 0;
        anchorSmoothScroll.scrollTo();
      });

      $rootScope.$on('$stateChangeError', 
        function(event, toState, toParams, fromState, fromParams, error) {
        // We can catch the error thrown when the $requireAuth promise is rejected
        // and redirect the user back to the home page
        if (error === 'AUTH_REQUIRED') {
          $state.go('app.lc.home');
        }
      });

      formlyConfig.setWrapper({
        name: 'validation',
        types: ['input', 'textarea'],
        templateUrl: 'template/messages.html'
      });

      formlyValidationMessages.addTemplateOptionValueMessage('maxlength', 'maxlength', '', 'es la longitud máxima');
      formlyValidationMessages.addTemplateOptionValueMessage('minlength', 'minlength', '', 'es la longitud mínima');
      formlyValidationMessages.addTemplateOptionValueMessage('max', 'max', '', 'es el valor máximo');
      formlyValidationMessages.addTemplateOptionValueMessage('min', 'min', '', 'es el valor mínimo');
      formlyValidationMessages.messages.required = 'to.label + " es un campo requerido"';
      formlyValidationMessages.messages.pattern = '"Formato inválido"';
      formlyValidationMessages.messages.number = 'to.label + " debe ser un número"';
      formlyValidationMessages.messages.email = '$viewValue + " no es un email válido"';
    })

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider,
      $locationProvider, LogglyLoggerProvider, LoggyToken) {
      
      $httpProvider.interceptors.push('AuthInterceptor');
      $locationProvider.hashPrefix('!');
      $locationProvider.html5Mode(false);
      
      LogglyLoggerProvider
        .inputToken(LoggyToken)
        .sendConsoleErrors(true)
        .includeTimestamp(true);
    });
})();
